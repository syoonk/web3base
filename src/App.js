import { MobxProvider } from "./hooks/useMobxStore.js";
import WalletConnector from "./actions/WalletConnector";

function App() {
  return (
    <div style={{paddingBottom: "50px"}}>
      <MobxProvider>
        <WalletConnector></WalletConnector>
      </MobxProvider>
    </div>
  );
}

export default App;
