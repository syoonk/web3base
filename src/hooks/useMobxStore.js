import { useContext } from 'react';

import { ProviderContext, Web3Provider } from "../contexts/providerContext";

export const useMobxStore = () => {
  const ProviderStore = useContext(ProviderContext);
  
  return { ProviderStore };
}

export const MobxProvider = (props) => {
  return (
    <Web3Provider>
      {props.children}
    </Web3Provider> 
  )
}
