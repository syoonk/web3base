import WalletConnectProvider from "@walletconnect/web3-provider";
// import Fortmatic from "fortmatic";
// import Portis from "@portis/web3";
import Web3Modal from "web3modal";

export const Endpoints = {
  1: process.env.REACT_APP_INFURA_MAINNET,
  42: process.env.REACT_APP_INFURA_KOVAN
}

export const INITIAL_STATE = {
  address: "",
  provider: null,
  connected: false,
  chainId: 1,
  networkId: 1,
  web3: Endpoints[1],
  contracts: null
};

export const providerOptions = {
  walletconnect: {
    package: WalletConnectProvider, // required
    options: {
      infuraId: "8a313bcfaadd4f6384598e705151f606" // required
    }
  },
  // fortmatic: {
  //   package: Fortmatic, // required
  //   options: {
  //     key: "pk_test_CC64BBA7717E4E70" // required
  //   }
  // },
  // portis: {
  //   package: Portis, // required
  //   options: {
  //     id: "PORTIS_ID" // required
  //   }
  // }
};

export const web3Modal = new Web3Modal({
  cacheProvider: true,
  providerOptions: providerOptions,
  theme: {
    background: "rgb(39, 49, 56)",
    main: "rgb(199, 199, 199)",
    secondary: "rgb(136, 136, 136)",
    border: "rgba(195, 195, 195, 0.14)",
    hover: "rgb(16, 26, 32)"
  }
});
